# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :frontend, FrontendWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "gk8WLHoRSWCubQ6VeOQP1VtZpHHzfysxpK3XiL1w/ne7gwczH0MVhB1m0onv8QJT",
  render_errors: [view: FrontendWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Frontend.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :frontend, :api_host,
  host: System.get_env("BACKEND_HOST"),
  port: System.get_env("BACKEND_PORT"),
  protocol: System.get_env("BACKEND_PROTOCOL"),
  base_api_path: System.get_env("BASE_API_PATH")


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
