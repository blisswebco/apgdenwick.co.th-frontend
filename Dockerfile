FROM elixir:1.5.3
RUN apt update
RUN apt install -y inotify-tools curl apt-transport-https build-essential
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN curl -sL https://deb.nodesource.com/setup_9.x | bash -
RUN apt install yarn nodejs
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir -p /frontend
WORKDIR /frontend
ADD . /frontend
WORKDIR /frontend/assets
RUN yarn global add elm --prefix /usr/local
RUN yarn global add elm-test --prefix /usr/local
RUN yarn install
RUN yarn deploy
WORKDIR /frontend
ENV PORT 80
ENV MIX_ENV prod
RUN mix deps.get --only prod
CMD mix phx.server
