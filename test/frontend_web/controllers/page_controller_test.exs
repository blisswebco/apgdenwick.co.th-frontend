defmodule FrontendWeb.PageControllerTest do
  use FrontendWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "A.P.G. Denwick Co., Ltd. Website"
  end

  test "GET /executive-summaries", %{conn: conn} do
    conn = get conn, "/executive-summaries"
    assert html_response(conn, 200) =~ "A.P.G. Denwick Co., Ltd. Website"
  end

  test "GET /past-performances", %{conn: conn} do
    conn = get conn, "/past-performances"
    assert html_response(conn, 200) =~ "A.P.G. Denwick Co., Ltd. Website"
  end

  test "GET /past-performances/slug", %{conn: conn} do
    conn = get conn, "/past-performances"
    assert html_response(conn, 200) =~ "A.P.G. Denwick Co., Ltd. Website"
  end

  test "GET /compnay-information", %{conn: conn} do
    conn = get conn, "company-information"
    assert html_response(conn, 200) =~ "A.P.G. Denwick Co., Ltd. Website"
  end
end
