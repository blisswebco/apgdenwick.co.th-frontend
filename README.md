# A.P.G. Denwick Project
## Frontend

### Current Docker image blisscs/apg-frontend:0.0.11

### Staging environment: https://apg-frontend.herokuapp.com/

### Setup
1. Clone the project
2. To further develop `docker-compose up`
3. goto localhost:4000/admin and input some initial data
4. goto localhost:5000 to use it

### TODO

* change title field in past performance items to details field
* Set Appropriate page title for each route and update the test accordingly
* Add Company Information Page and set a link to it
* Add Sitemap details
* Update Past Performances Page
* Update Past Performance page
* test GET /past-performances/:slug (server side)


### Done
* change field title to details in past performance items
* Fix doing elm-test in bitbucket.
Error "(node:45) UnhandledPromiseRejectionWarning: Unhandled promise rejection (rejection id: 1): Error: EISDIR: illegal operation on a directory, read"

### Recommendation

* Use app version as same as docker image version.
