defmodule FrontendWeb.LayoutView do
  use FrontendWeb, :view

  def api_host do
    Application.get_env(:frontend, :api_host)
  end
end
