import "bootstrap-loader"

const Elm = require("../elm/App.elm");
const elmNode = document.getElementById("elm-wrapper");
const apiEndpoint = elmNode.getAttribute('data-api-host');

const ElmApp = Elm.App.embed(elmNode, {apiEndpoint: apiEndpoint})

