module View exposing (view)

import View.Home as HomeView
import View.ExecutiveSummaries as ExecutiveSummariesView
import View.PastPerformances as PastPerformancesView
import View.PastPerformance as PastPerformanceView
import View.CompanyInformation as CompanyInformationView
import View.NotFound as NotFoundView
import Model exposing (Model)
import Route exposing (Route(..), Route)
import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (Msg(..))
import Helpers exposing (onClickPage)


view : Model -> Html Msg
view model =
    case model.route of
        Just Home ->
            HomeView.view model

        _ ->
            div [ class "container-fluid" ]
                [ div [ class "row" ]
                    [ div [ class "col" ]
                        [ h1 [ class "text-center display-4" ] [ text "A.P.G. Denwick Co., Ltd." ]
                        , h1 [ class "text-center lead" ] [ text "บ.จ.ก. เอ.พี.จี. เดนวิก" ]
                        ]
                    ]
                , case model.route of
                    Just Home ->
                        Debug.crash "Not posibble"

                    Nothing ->
                        NotFoundView.view

                    Just CompanyInformationPage ->
                        CompanyInformationView.view

                    Just ExecutiveSummariesPage ->
                        ExecutiveSummariesView.view model.executiveSummaries model.selectedExecutiveSummary

                    Just PastPerformancesPage ->
                        PastPerformancesView.view model.pastPerformances

                    Just (PastPerformancePage slug) ->
                        PastPerformanceView.view
                , p [ class "text-right" ] [ a ((onClickPage Home) ++ [ class "btn btn-outline-secondary" ]) [ text "<- Back to Home Page" ] ]
                , hr [] []
                , p [ class "lead" ]
                    [ text "บ.จ.ก. เอ.พี.จี. เดนวิก"
                    , br [] []
                    , text "330/4 ซอยพานิชอนันต์, ถ. สุขุมวิท 71"
                    , br [] []
                    , text "แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพฯ 10110"
                    ]
                , h2 [ class "lead" ] [ text "Sitemap" ]
                ]
