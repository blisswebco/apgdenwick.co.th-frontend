module Helpers exposing (onClickPage)

import Route exposing (Route(..), routeToString)
import Html.Attributes exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Json.Decode exposing (..)
import Msg exposing (Msg(..))


onClickPage : Route -> List (Attribute Msg)
onClickPage route =
    [ style [("pointer", "cursor")]
    , href (routeToString route)
    , onPreventDefaultClick (UpdateUrl (routeToString route))
    ]

onPreventDefaultClick : msg -> Attribute msg
onPreventDefaultClick message =
    onWithOptions "click"
        { defaultOptions | preventDefault = True }
        (preventDefault2
        |> Json.Decode.andThen (maybePreventDefault message)
        )
        
preventDefault2 : Decoder Bool
preventDefault2 =
    Json.Decode.map2
        (invertedOr)
        (Json.Decode.field "ctrlKey" Json.Decode.bool)
        (Json.Decode.field "metaKey" Json.Decode.bool)


maybePreventDefault : msg -> Bool -> Decoder msg
maybePreventDefault msg preventDefault =
    case preventDefault of
        True ->
            Json.Decode.succeed msg

        False ->
            Json.Decode.fail "Normal link"


invertedOr : Bool -> Bool -> Bool
invertedOr x y =
    not (x || y)
