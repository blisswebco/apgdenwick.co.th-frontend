module Decode.PastPerformance exposing (..)

import Model.PastPerformance exposing (PastPerformance)
import Json.Decode exposing (string, Decoder, int, list)
import Json.Decode.Pipeline exposing (decode, required)

pastPerformanceDecoder : Decoder PastPerformance
pastPerformanceDecoder =
    decode PastPerformance
        |> required "id" int
        |> required "name" string
        |> required "slug" string

type alias PastPerformances =
    { past_performances : List PastPerformance
    }

pastPerformancesDecoder : Decoder PastPerformances
pastPerformancesDecoder =
    decode PastPerformances
        |> required "past_performances" (list pastPerformanceDecoder)
