module Decode.ExecutiveSummary exposing (..)

import Model.ExecutiveSummary exposing (ExecutiveSummary)
import Json.Decode exposing (string, Decoder, int, list, nullable)
import Json.Decode.Pipeline exposing (decode, required)

executiveSummaryDecoder : Decoder ExecutiveSummary
executiveSummaryDecoder =
    decode ExecutiveSummary
        |> required "id" int
        |> required "order" int
        |> required "name" string
        |> required "age" int
        |> required "gender" string
        |> required "position" string
        |> required "other_info" (nullable string)

type alias ExecutiveSummaries =
    { executive_summaries : List ExecutiveSummary
    }

executiveSummariesDecoder : Decoder ExecutiveSummaries
executiveSummariesDecoder =
    decode ExecutiveSummaries
        |> required "executive_summaries" (list executiveSummaryDecoder)
