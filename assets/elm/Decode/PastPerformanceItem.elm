module Decode.PastPerformanceItem exposing (..)

import Model.PastPerformanceItem exposing (PastPerformanceItem)
import Json.Decode exposing (string, Decoder, int, list)
import Json.Decode.Pipeline exposing (decode, required)


pastPerformanceItemDecoder : Decoder PastPerformanceItem
pastPerformanceItemDecoder =
    decode PastPerformanceItem
        |> required "id" int
        |> required "details" string
        |> required "past_performance_id" int


type alias PastPerformanceItems =
    { past_performance_items : List PastPerformanceItem
    }


pastPerformanceItemsDecoder : Decoder PastPerformanceItems
pastPerformanceItemsDecoder =
    decode PastPerformanceItems
        |> required "past_performance_items" (list pastPerformanceItemDecoder)
