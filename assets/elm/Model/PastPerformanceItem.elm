module Model.PastPerformanceItem exposing (PastPerformanceItem)

type alias PastPerformanceItem =
    { id : Int
    , title : String
    , past_performance_id : Int
    }
        
