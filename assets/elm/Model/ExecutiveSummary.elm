module Model.ExecutiveSummary exposing (ExecutiveSummary)

type alias ExecutiveSummary =
    { id : Int
    , order : Int
    , name : String
    , age : Int
    , gender : String
    , position : String
    , other_info : Maybe String
    }
