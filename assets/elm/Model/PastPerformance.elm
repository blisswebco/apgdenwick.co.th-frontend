module Model.PastPerformance exposing (PastPerformance)

type alias PastPerformance =
    { id : Int
    , name: String
    , slug: String
    }
