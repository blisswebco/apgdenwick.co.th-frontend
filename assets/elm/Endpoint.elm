module Endpoint exposing ( executiveSummaries
                         , pastPerformances
                         , pastPerformanceItems
                         )

executiveSummaries : String -> String
executiveSummaries base =
    base ++ "/executive_summaries"

pastPerformances : String -> String
pastPerformances base =
    base ++ "/past_performances"

pastPerformanceItems : String -> String
pastPerformanceItems base =
    base ++ "/past_performance_items"
