module Msg exposing (Msg(..))

import Navigation exposing (Location)
import Decode.ExecutiveSummary exposing (ExecutiveSummaries)
import Decode.PastPerformance exposing (PastPerformances)
import Decode.PastPerformanceItem exposing (PastPerformanceItems)
import Model.ExecutiveSummary exposing (ExecutiveSummary)
import RemoteData exposing (WebData)


type Msg
    = UpdateContentAfterUrlChange Location
    | UpdateUrl String
    | OnFetchExecutiveSummaries (WebData ExecutiveSummaries)
    | OnFetchPastPerformances (WebData PastPerformances)
    | OnFetchPastPerformanceItems (WebData PastPerformanceItems)
    | USERSELECTEDEXECUTIVESUMMARY ExecutiveSummary
