module Route exposing (Route(..), route, routeToString)

import UrlParser as Url exposing ((</>), (<?>), s, int, stringParam, top)


type Route
    = Home
    | ExecutiveSummariesPage
    | PastPerformancesPage
    | PastPerformancePage String
    | CompanyInformationPage


route : Url.Parser (Route -> a) a
route =
    Url.oneOf
        [ Url.map Home top
        , Url.map ExecutiveSummariesPage (s "executive-summaries")
        , Url.map PastPerformancesPage (s "past-performances")
        , Url.map CompanyInformationPage (s "company-information")
        , Url.map PastPerformancePage <|
            s "past-performances"
                </> Url.string
        ]


routeToString : Route -> String
routeToString route =
    let
        pieces =
            case route of
                Home ->
                    []

                ExecutiveSummariesPage ->
                    [ "executive-summaries" ]

                PastPerformancesPage ->
                    [ "past-performances" ]

                PastPerformancePage slug ->
                    [ "past-performances" ++ "/" ++ slug ]

                CompanyInformationPage ->
                    [ "company-information" ]
    in
        "/" ++ (String.join "/" pieces)
