module Update exposing (update)

import Msg exposing (Msg(..), Msg)
import Model
    exposing
        ( Model
        , setExecutiveSummaries
        , setPastPerformances
        , setPastPerformanceItems
        , setSelectedExecutiveSummary
        , setRoute
        )
import Navigation
import RemoteData exposing (WebData)
import UrlParser exposing (..)
import Route exposing (route)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateUrl newUrl ->
            ( model, Navigation.newUrl newUrl )

        UpdateContentAfterUrlChange location ->
            ( setRoute (parsePath route location) model, Cmd.none )

        OnFetchExecutiveSummaries response ->
            let
                executiveSummaries =
                    (RemoteData.map .executive_summaries response)

                selectedExecutiveSummary =
                    case executiveSummaries of
                        RemoteData.Success executiveSummaries ->
                            List.head executiveSummaries

                        _ ->
                            Nothing
            in
                ( setSelectedExecutiveSummary selectedExecutiveSummary <| setExecutiveSummaries executiveSummaries model
                , Cmd.none
                )

        OnFetchPastPerformances response ->
            ( setPastPerformances (RemoteData.map .past_performances response) model
            , Cmd.none
            )

        OnFetchPastPerformanceItems response ->
            ( setPastPerformanceItems (RemoteData.map .past_performance_items response) model
            , Cmd.none
            )

        USERSELECTEDEXECUTIVESUMMARY executiveSummary ->
            ( setSelectedExecutiveSummary (Just executiveSummary) model
            , Cmd.none
            )
