module Model
    exposing
        ( Model
        , setExecutiveSummaries
        , setPastPerformances
        , setPastPerformanceItems
        , setRoute
        , setSelectedExecutiveSummary
        , PastPerformances
        , ExecutiveSummaries
        )

import Model.ExecutiveSummary exposing (ExecutiveSummary)
import Model.PastPerformance exposing (PastPerformance)
import Model.PastPerformanceItem exposing (PastPerformanceItem)
import RemoteData exposing (WebData)
import Route exposing (Route)


type alias Model =
    { apiEndpoint : String
    , executiveSummaries : WebData (List ExecutiveSummary)
    , pastPerformances : WebData (List PastPerformance)
    , pastPerformanceItems : WebData (List PastPerformanceItem)
    , selectedExecutiveSummary : Maybe ExecutiveSummary
    , route : Maybe Route
    }


type alias SelectedExecutiveSummary a =
    { a
        | selectedExecutiveSummary : Maybe ExecutiveSummary
    }


setSelectedExecutiveSummary :
    Maybe ExecutiveSummary
    -> SelectedExecutiveSummary a
    -> SelectedExecutiveSummary a
setSelectedExecutiveSummary maybeExecutiveSummary model =
    { model | selectedExecutiveSummary = maybeExecutiveSummary }


type alias ExecutiveSummaries a =
    { a
        | executiveSummaries : WebData (List ExecutiveSummary)
    }


setExecutiveSummaries :
    WebData (List ExecutiveSummary)
    -> ExecutiveSummaries a
    -> ExecutiveSummaries a
setExecutiveSummaries webData model =
    { model
        | executiveSummaries = webData
    }


type alias PastPerformances a =
    { a
        | pastPerformances : WebData (List PastPerformance)
    }


setPastPerformances :
    WebData (List PastPerformance)
    -> PastPerformances a
    -> PastPerformances a
setPastPerformances webData model =
    { model
        | pastPerformances = webData
    }


type alias PastPerformanceItems a =
    { a
        | pastPerformanceItems : WebData (List PastPerformanceItem)
    }


setPastPerformanceItems :
    WebData (List PastPerformanceItem)
    -> PastPerformanceItems a
    -> PastPerformanceItems a
setPastPerformanceItems webData model =
    { model
        | pastPerformanceItems = webData
    }


type alias RouteModel a =
    { a | route : Maybe Route }


setRoute : Maybe Route -> RouteModel a -> RouteModel a
setRoute route model =
    { model
        | route = route
    }
