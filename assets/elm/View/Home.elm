module View.Home exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import RemoteData
import Model
    exposing
        ( Model
        , ExecutiveSummaries
        , PastPerformances
        )
import Model.PastPerformance exposing (PastPerformance)
import Msg exposing (Msg)
import Route exposing (Route(..))
import Helpers exposing (onClickPage)


view : Model -> Html Msg
view model =
    div [ class "container-fluid" ]
        [ div [ class "row" ]
            [ div [ class "col" ]
                [ h1 [ class "text-center display-4" ] [ text "A.P.G. Denwick., Ltd." ]
                , h1 [ class "text-center lead" ] [ text "บ.จ.ก. เอ.พี.จี. เดนวิก" ]
                ]
            , div [ class "col" ]
                [ p [ class "lead text-right" ]
                    [ text "บ.จ.ก. เอ.พี.จี. เดนวิก"
                    , br [] []
                    , text "330/4 ซอยพานิชอนันต์, ถ. สุขุมวิท 71"
                    , br [] []
                    , text "แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพฯ 10110"
                    ]
                ]
            ]
        , div [ class "row" ]
            [ div [ class "col" ]
                [ h2 [ class "text-center display-5" ] [ text "General Information" ]
                , generalInformationView model
                , h2 [ class "text-center display-5" ] [ text "Past Peformance" ]
                , pastPerformanceView model
                ]
            ]
        , div [ class "row" ]
            [ div [ class "col" ]
                [ h2 [ class "text-center display-5" ] [ text "Main Projects" ]
                ]
            ]
        , div [ class "row" ]
            [ div [ class "col" ]
                [ p [ class "text-center" ]
                    [ text "Develop By Suracheth Chawla"
                    , br [] []
                    , text "Line Contract : @blissweb"
                    ]
                ]
            ]
        ]


generalInformationView : ExecutiveSummaries a -> Html Msg
generalInformationView model =
    div [ class "row" ]
        [ div [ class "col" ]
            [ a ((onClickPage CompanyInformationPage) ++ [ class "btn btn-block btn-outline-info lead" ]) [ text "Company Information" ] ]
        , div [ class "col" ]
            [ case model.executiveSummaries of
                RemoteData.NotAsked ->
                    text "Waiting for Executive Summaries to be Fetched!"

                RemoteData.Loading ->
                    text "Loading ..."

                RemoteData.Failure e ->
                    text (toString e)

                RemoteData.Success executiveSummaries ->
                    a ((onClickPage ExecutiveSummariesPage) ++ [ class "btn btn-block btn-outline-info lead" ]) [ text "Executive Summaries" ]
            ]
        ]


pastPerformanceView : PastPerformances a -> Html Msg
pastPerformanceView model =
    case model.pastPerformances of
        RemoteData.NotAsked ->
            text "Waiting for Past Performances Information to be Fetched!"

        RemoteData.Loading ->
            text "Loading ..."

        RemoteData.Failure e ->
            text (toString e)

        RemoteData.Success pastPerformances ->
            div [ class "row" ] <|
                List.map showPerformance pastPerformances


showPerformance : PastPerformance -> Html Msg
showPerformance pastPerformance =
    div [ class "col" ]
        [ a
            ((onClickPage <| PastPerformancePage (.slug pastPerformance))
                ++ [ class "btn btn-block btn-outline-info lead" ]
            )
            [ text <| .name pastPerformance ]
        ]
