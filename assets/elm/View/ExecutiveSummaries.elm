module View.ExecutiveSummaries exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (Msg(..))
import Model.ExecutiveSummary exposing (ExecutiveSummary)
import RemoteData exposing (WebData)
import HtmlParser as HtmlParser exposing (parse)
import HtmlParser.Util as Util exposing (toVirtualDom)
import Html.Events exposing (onClick)


view : WebData (List ExecutiveSummary) -> Maybe ExecutiveSummary -> Html Msg
view executiveSummaries selectedExecutiveSummary =
    case executiveSummaries of
        RemoteData.Loading ->
            text "Loading"

        RemoteData.NotAsked ->
            text "Waiting to Load!"

        RemoteData.Failure error ->
            text <| toString error

        RemoteData.Success executiveSummaries ->
            div []
                [ h2 [ class "lead" ] [ text "Executive Summaries" ]
                , div [ class "row" ]
                    [ div [ class "col-md-3" ]
                        [ ul [ class "nav flex-column" ] <|
                            List.map (displayExecutiveSummaryLink selectedExecutiveSummary) executiveSummaries
                        ]
                    , div [ class "col" ]
                        [ displayExecutiveSummary selectedExecutiveSummary ]
                    ]
                ]


displayExecutiveSummaryLink : Maybe ExecutiveSummary -> ExecutiveSummary -> Html Msg
displayExecutiveSummaryLink selectedExecutiveSummary executiveSummary =
    li []
        [ button
            [ class <|
                "btn btn-block"
                    ++ if selectedExecutiveSummary == (Just executiveSummary) then
                        " btn-info"
                       else
                        " btn-outline-info"
            , onClick <| USERSELECTEDEXECUTIVESUMMARY executiveSummary
            ]
            [ text <| .name executiveSummary ]
        ]


displayExecutiveSummary : Maybe ExecutiveSummary -> Html Msg
displayExecutiveSummary maybeExecutiveSummary =
    div [] <|
        case maybeExecutiveSummary of
            Just executiveSummary ->
                [ h3 [ class "display-4" ] [ text <| "Name: " ++ .name executiveSummary ]
                , h4 [ class "lead" ] [ text <| "Position: " ++ .position executiveSummary ]
                , div [ class "row" ]
                    [ div [ class "col" ] [ h4 [ class "lead" ] [ text <| "Age: " ++ toString (.age executiveSummary) ] ]
                    , div [ class "col" ] [ h4 [ class "lead" ] [ text <| "Gender: " ++ .gender executiveSummary ] ]
                    ]
                , case (.other_info executiveSummary) of
                    Just other_info ->
                        div [] <| toVirtualDom <| parse other_info

                    Nothing ->
                        text ""
                ]

            Nothing ->
                []
