module View.CompanyInformation exposing (view)

import Msg exposing (Msg(..))
import Html exposing (..)
import Html.Attributes exposing (..)


view : Html Msg
view =
    div []
        [ h2 [ class "lead" ] [ text "Company Information" ]
        , div [ class "row" ]
            [ div [ class "col" ]
                [ table [ class "table table-striped" ]
                    [ tbody []
                        [ tr []
                            [ th [] [ text "Company Name" ]
                            , td [] [ text "A.P.G Denwick Co., Ltd." ]
                            ]
                        , tr []
                            [ th [] [ text "Location" ]
                            , td []
                                [ text "330/4 Soi Panichanan Sukhumvit 71 Rd.,"
                                , br [] []
                                , text "Klongton-Nua, Wattana, Bangkok 10110"
                                , br [] []
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Registration Date" ]
                            , td []
                                [ text "October 7, 2002"
                                ]
                            ]
                        , tr
                            []
                            [ th [] [ text "Registration Number" ]
                            , td []
                                [ text "0105545105742"
                                ]
                            ]
                        , tr
                            []
                            [ th [] [ text "Registration Capital" ]
                            , td []
                                [ text "Baht 10,000,000"
                                ]
                            ]
                        , tr
                            []
                            [ th [] [ text "Directors" ]
                            , td []
                                [ ul []
                                    [ li []
                                        [ text "Amrit Srikuruwan" ]
                                    , li
                                        []
                                        [ text "Prasert Srikuruwan" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Head Engineers" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Mr. Mr.Samai Boonraksa" ]
                                    , li [] [ text "Mr. Patiwat Panrod" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Head Architects" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Mr.Woravit Thamyotskul" ]
                                    , li [] [ text "Mr.Sirpong Premrattna" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Head MEPs" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Mr. Sampan Keawkarn" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Business Types" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Construction of all kinds of low & high rise building" ]
                                    , li [] [ text "Construction of residential houses" ]
                                    , li [] [ text "Construction of roads and V ditch" ]
                                    , li [] [ text "Pavement construction" ]
                                    , li [] [ text "Factory contractor" ]
                                    , li [] [ text "Warehouse contractor" ]
                                    , li [] [ text "Construction of complete infrastructure" ]
                                    , li [] [ text "Construction and maintenance for all kinds of construction" ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            , div [ class "col" ]
                [ table [ class "table table-striped" ]
                    [ tbody []
                        [ tr []
                            [ th [] [ text "ชื่อ" ]
                            , td [] [ text "บ.จ.ก. เอ.พี.จี เดนวิก" ]
                            ]
                        , tr []
                            [ th [] [ text "สถานที่ตั้ง" ]
                            , td []
                                [ text "330/4 ซอยพานิชอนันต์ ถนนสุขุมวิท 71,"
                                , br [] []
                                , text "แขวงคลองตันเหนือ เขตวัฒนา กรุงเทพฯ 10110"
                                , br [] []
                                ]
                            ]
                        , tr []
                            [ th [] [ text "วันจดทะเบียน" ]
                            , td []
                                [ text "วันที่ 7 ตุลาคม พ.ศ. 2545"
                                ]
                            ]
                        , tr
                            []
                            [ th [] [ text "เลขทะเบียน ผู้เสียภาษี" ]
                            , td []
                                [ text "0105545105742"
                                ]
                            ]
                        , tr
                            []
                            [ th [] [ text "ทุนจดทะเบียน" ]
                            , td []
                                [ text "10,000,000 บาท"
                                ]
                            ]
                        , tr
                            []
                            [ th [] [ text "ผู้บริหาร" ]
                            , td []
                                [ ul []
                                    [ li []
                                        [ text "นายอัมฤทธิ์ ศรีคุรุวาฬ" ]
                                    , li
                                        []
                                        [ text "นายประเสริฐ ศรีคุรุวาฬ" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Head Engineers" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Mr. Samai Boonraksa" ]
                                    , li [] [ text "Mr. Patiwat Panrod" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Head Architects" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Mr.Woravit Thamyotskul" ]
                                    , li [] [ text "Mr.Sirpong Premrattna" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Head MEPs" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Mr. Sampan Keawkarn" ]
                                    ]
                                ]
                            ]
                        , tr []
                            [ th [] [ text "Business Types" ]
                            , td []
                                [ ul []
                                    [ li [] [ text "Construction of all kinds of low & high rise building" ]
                                    , li [] [ text "Construction of residential houses" ]
                                    , li [] [ text "Construction of roads and V ditch" ]
                                    , li [] [ text "Pavement construction" ]
                                    , li [] [ text "Factory contractor" ]
                                    , li [] [ text "Warehouse contractor" ]
                                    , li [] [ text "Construction of complete infrastructure" ]
                                    , li [] [ text "Construction and maintenance for all kinds of construction" ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
