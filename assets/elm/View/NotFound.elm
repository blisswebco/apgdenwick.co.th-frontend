module View.NotFound exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (Msg(..))

view : Html Msg
view =
    text "The page you are looking could not be found! "
