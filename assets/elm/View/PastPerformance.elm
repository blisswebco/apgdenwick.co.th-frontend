module View.PastPerformance exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Msg exposing (Msg(..))

view : Html Msg
view = text "Past Performance Slug Page"
