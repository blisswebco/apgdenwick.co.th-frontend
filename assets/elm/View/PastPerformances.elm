module View.PastPerformances exposing (view)

import Html exposing (..)
import Msg exposing (Msg(..))
import Model.PastPerformance exposing (PastPerformance)
import RemoteData exposing (WebData)
import Route exposing (Route(..))
import Helpers exposing (onClickPage)

view : WebData (List PastPerformance) -> Html Msg
view pastPerformances =
    case pastPerformances of
        RemoteData.Loading -> text "Loading"
        RemoteData.NotAsked -> text "Waiting to Load!"
        RemoteData.Failure error -> text (toString error)
        RemoteData.Success pastPerformances ->
            div []
                [ h2 [] [text "Past Performances"]
                , ul [] (List.map
                             (\pastPerformance ->
                                  li []
                                      [ a (onClickPage <| PastPerformancePage <| .slug pastPerformance)
                                            [text <| .name pastPerformance]
                                      ])
                             pastPerformances)
                ]
