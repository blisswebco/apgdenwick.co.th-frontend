module App exposing (main)

import Msg exposing (Msg(..))
import Navigation exposing (programWithFlags, Location)
import Model exposing (Model)
import RemoteData exposing (WebData)
import View exposing (view)
import Update exposing (update)
import Decode.ExecutiveSummary as ExecutiveSummaryDecode
import Decode.PastPerformance as PastPerformanceDecode
import Decode.PastPerformanceItem as PastPerformanceItemDecode
import Endpoint
import Http
import Route exposing (Route(..), route)
import UrlParser exposing (..)


main : Program Flags Model Msg
main =
    programWithFlags UpdateContentAfterUrlChange
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias Flags =
    { apiEndpoint : String }


init : Flags -> Navigation.Location -> ( Model, Cmd Msg )
init flags location =
    let
        apiEndpoint =
            flags.apiEndpoint
    in
        ( { apiEndpoint = apiEndpoint
          , executiveSummaries = RemoteData.Loading
          , pastPerformances = RemoteData.Loading
          , pastPerformanceItems = RemoteData.Loading
          , route = parsePath route location
          , selectedExecutiveSummary = Nothing
          }
        , Cmd.batch
            [ fetchExecutiveSummaries apiEndpoint
            , fetchPastPerformances apiEndpoint
            , fetchPastPerformanceItems apiEndpoint
            ]
        )


subscriptions : Model -> Sub msg
subscriptions model =
    Sub.none


fetchExecutiveSummaries : String -> Cmd Msg
fetchExecutiveSummaries apiEndpoint =
    Http.get
        (Endpoint.executiveSummaries apiEndpoint)
        ExecutiveSummaryDecode.executiveSummariesDecoder
        |> RemoteData.sendRequest
        |> Cmd.map OnFetchExecutiveSummaries


fetchPastPerformances : String -> Cmd Msg
fetchPastPerformances apiEndpoint =
    Http.get
        (Endpoint.pastPerformances apiEndpoint)
        PastPerformanceDecode.pastPerformancesDecoder
        |> RemoteData.sendRequest
        |> Cmd.map OnFetchPastPerformances


fetchPastPerformanceItems : String -> Cmd Msg
fetchPastPerformanceItems apiEndpoint =
    Http.get
        (Endpoint.pastPerformanceItems apiEndpoint)
        PastPerformanceItemDecode.pastPerformanceItemsDecoder
        |> RemoteData.sendRequest
        |> Cmd.map OnFetchPastPerformanceItems
